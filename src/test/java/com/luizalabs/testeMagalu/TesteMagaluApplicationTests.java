package com.luizalabs.testeMagalu;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.luizalabs.testeMagalu.dominio.BuscaCEPResponse;
import com.luizalabs.testeMagalu.dominio.Endereco;
import com.luizalabs.testeMagalu.interfaces.BuscaCEPRestController;

@SpringBootTest
class TesteMagaluApplicationTests {
	@Autowired
	BuscaCEPRestController buscaCEPRestController;

	@Test
	void contextLoads() {
	}

	@Test
	void testBuscaCEPNotNull() throws Exception {
		assertNotNull(buscaCEPRestController.buscaCEP("02408-110"));
	}

	@Test
	void testBuscaCEPValido() throws Exception {
		BuscaCEPResponse buscaCEPResponseModelo = new BuscaCEPResponse();
		buscaCEPResponseModelo.setEndereco(
				new Endereco("02408-110", "Rua Dr Artur Leite de Barros Jr", "Água Fria", "São Paulo", "São Paulo"));

		assertThat(buscaCEPResponseModelo.equals(buscaCEPRestController.buscaCEP("02408-110")));
	}

	@Test
	void testBuscaCEPNaoEncontrado() throws Exception {
		BuscaCEPResponse buscaCEPResponseModelo = new BuscaCEPResponse();
		buscaCEPResponseModelo.setMensagem("CEP não encontrado");

		assertThat(buscaCEPResponseModelo.equals(buscaCEPRestController.buscaCEP("02408190")));
	}

	@Test
	void testBuscaCEPInvalido() throws Exception {
		BuscaCEPResponse buscaCEPResponseModelo = new BuscaCEPResponse();
		buscaCEPResponseModelo.setMensagem("CEP Inválido");

		assertThat(buscaCEPResponseModelo.equals(buscaCEPRestController.buscaCEP("teste")));
		assertThat(buscaCEPResponseModelo.equals(buscaCEPRestController.buscaCEP("024500000")));
	}

	@Test
	void testBuscaCEPValidoInexistente() throws Exception {
		BuscaCEPResponse buscaCEPResponseModelo = new BuscaCEPResponse();
		buscaCEPResponseModelo.setEndereco(
				new Endereco("10000-000", "Teste CEP Válido Inexistente", "Água Fria", "São Paulo", "São Paulo"));

		assertThat(buscaCEPResponseModelo.equals(buscaCEPRestController.buscaCEP("11111111")));
	}

}
