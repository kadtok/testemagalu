package com.luizalabs.testeMagalu.negocio;

import org.springframework.stereotype.Service;

import com.luizalabs.testeMagalu.dominio.BuscaCEPResponse;

@Service
public interface BuscaCEPService {

	BuscaCEPResponse buscaEndereco(String cep);

}
