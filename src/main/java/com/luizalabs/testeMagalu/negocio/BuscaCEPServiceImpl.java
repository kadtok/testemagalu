package com.luizalabs.testeMagalu.negocio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luizalabs.testeMagalu.dominio.BuscaCEPResponse;
import com.luizalabs.testeMagalu.persistencia.EnderecoDAO;

@Service
public class BuscaCEPServiceImpl implements BuscaCEPService {

	@Autowired
	private EnderecoDAO enderecoDAO;

	@Override
	public BuscaCEPResponse buscaEndereco(String cep) {

		cep = cep.replace("-", "");
		BuscaCEPResponse retorno = new BuscaCEPResponse();

		if (isCEPValido(cep)) {
			buscarEnderecoCEP(cep, retorno);
		} else {
			retorno.setMensagem("CEP Inválido");
		}

		return retorno;
	}

	/**
	 * Verifica se o CEP informado contem letras e se a quantidade de caracteres
	 * está coerente.
	 * 
	 * @param cep
	 * @return
	 */
	private boolean isCEPValido(String cep) {

		if (cep.length() != 8)
			return false;

		try {
			Integer.parseInt(cep);
		} catch (Exception e) {
			return false;

		}
		return true;
	}

	/**
	 * Busca o endereço do CEP informado.
	 * 
	 * @param cep
	 * @param retorno
	 */
	private void buscarEnderecoCEP(String cep, BuscaCEPResponse retorno) {
		while (retorno.getEndereco() == null && !cep.endsWith("00000000")) {

			retorno.setEndereco(enderecoDAO.obterEndereco(cep));

			if (retorno.getEndereco() == null) {
				cep = alteraCEP(cep);
			}

		}
		if (retorno.getEndereco() == null)
			retorno.setMensagem("Endereço não encontrado");
		;
	}

	/**
	 * Substitui o último algarismo diferente de zero no CEP informado.
	 * 
	 * @param cep
	 * @return
	 */
	private String alteraCEP(String cep) {
		int auxLoop = cep.length() - 1;
		while (cep.charAt(auxLoop) == '0') {
			auxLoop--;
		}
		cep = cep.substring(0, auxLoop) + "0" + cep.substring(auxLoop + 1);
		return cep;
	}

}
