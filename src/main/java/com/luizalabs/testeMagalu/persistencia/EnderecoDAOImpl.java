package com.luizalabs.testeMagalu.persistencia;

import java.util.HashMap;

import org.springframework.stereotype.Repository;

import com.luizalabs.testeMagalu.dominio.Endereco;

@Repository
public class EnderecoDAOImpl implements EnderecoDAO {

	/**
	 * Simula busca de endereço em um DB
	 * 
	 * @param cep
	 * @return
	 */
	@Override
	public Endereco obterEndereco(String cep) {

		HashMap<String, Endereco> cepsCadastrados = new HashMap<String, Endereco>();
		cepsCadastrados.put("02408110",
				new Endereco("02408-110", "Rua Dr Artur Leite de Barros Jr", "Água Fria", "São Paulo", "São Paulo"));
		cepsCadastrados.put("10000000",
				new Endereco("10000-000", "Teste CEP Válido Inexistente", "Água Fria", "São Paulo", "São Paulo"));
		cepsCadastrados.put("31170500",
				new Endereco("31170-500", "Rua Teodomiro Pereira", "União", "Belo Horizonte", "Minas Gerais"));
		cepsCadastrados.put("02047000",
				new Endereco("02047-000", "Rua Maria Prestes Maia", "Vila Guilherme", "São Paulo", "São Paulo"));
		cepsCadastrados.put("14400490",
				new Endereco("14400-490", "Rua Voluntários da Franca", "Centro", "Franca", "São Paulo"));

		return cepsCadastrados.get(cep);
	}

}
