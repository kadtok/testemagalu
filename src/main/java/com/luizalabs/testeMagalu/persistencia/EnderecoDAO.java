package com.luizalabs.testeMagalu.persistencia;

import com.luizalabs.testeMagalu.dominio.Endereco;

public interface EnderecoDAO {

	Endereco obterEndereco(String cep);

}
