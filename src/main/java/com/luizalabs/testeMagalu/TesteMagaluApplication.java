package com.luizalabs.testeMagalu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteMagaluApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteMagaluApplication.class, args);
	}

}
