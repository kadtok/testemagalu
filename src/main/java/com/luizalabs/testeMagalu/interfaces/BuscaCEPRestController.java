package com.luizalabs.testeMagalu.interfaces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.luizalabs.testeMagalu.dominio.BuscaCEPResponse;
import com.luizalabs.testeMagalu.negocio.BuscaCEPService;

@RestController
@RequestMapping(value = "/BuscaCEP")
public class BuscaCEPRestController {

	@Autowired
	private BuscaCEPService buscaCEPService;

	@RequestMapping(method = RequestMethod.GET)
	public BuscaCEPResponse buscaCEP(String cep) {
		return buscaCEPService.buscaEndereco(cep);

	}

}
